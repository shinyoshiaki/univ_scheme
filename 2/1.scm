(define zero 'zero)
(define succ (lambda (n) (list 'succ n)))
(define pred (lambda (n) (car (cdr n))))

(define plus
    (lambda (m n)
    (if(equal? n zero)
    m
    (succ (plus m (pred n))))))

(define mult
    (lambda(m n)
    (if (equal? n zero)
    zero
    (plus m (mult m (pred n))))))