;事前定義
(define zero 'zero)
(define one (succ zero))
(define two (succ one))
(define three (succ two))
(define succ (lambda (n) (list 'succ n)))
(define pred (lambda (n) (car (cdr n))))

;(1)加算
;m + Zero = m よりnがゼロのときにmを返す
;m + Succ n = Succ (m+n) よりnがsuccで進むから前の状態が必要なるのでpredを用いる
(define plus
    (lambda (m n)
    (if(equal? n zero)
    m
    (succ (plus m (pred n))))))
;実行結果
;> (plus two three)
;(succ (succ (succ (succ (succ zero)))))

;(2)乗算
;m * Zero = Zero よりnがゼロのときにmを返す
;m * Succ n =(m*n)+m より前の状態が必要なのでpredを用いる
(define mult
    (lambda(m n)
    (if (equal? n zero)
    zero
    (plus m (mult m (pred n))))))
;実行結果
;> (mult two three)
;(succ (succ (succ (succ (succ (succ zero))))))

;(3)foldn
;foldn  h c Zero = c よりnがゼロのときにcを返す
;foldn h c (Succ n) = h(foldn h c n) より前の状態が必要なのでpredを用いる     
(define foldn
    (lambda (h c n) 
        (if(equal? n zero)
        c
        (h (foldn h c (pred n))))))

;(3.1)
;"m + n =foldn Succ m n" の式を再現する
;実行結果
;> (foldn succ two three)
;(succ (succ (succ (succ (succ zero)))))

;(3.2)
;"m * n = foldn (+m) Zero n" の式を再現する
;plus関数は引数を2つ要するのでカリー化で引数を減らす
(define cplus
    (lambda (m) 
        (lambda(n)
        (plus m n))))
;実行結果
;> (foldn (cplus two) zero three)
;(succ (succ (succ (succ (succ (succ zero))))))