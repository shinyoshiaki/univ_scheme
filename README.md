//インタープリタ
sheme48

//読み込み
(load "*.scm")

//エラーレベル
ctrl + d 

//例
(cons obj1 obj2)
obj1を car部、obj2を cdr部の要素として持つペアを作る関数。

(car pair)
pairの 先頭を返す関数。

(cdr pair)
pairの 先頭以外を返す関数。


//トレース
> (load"1.scm")
> ,trace rev
> (rev '(1 2 3 4 5))
[Enter (rev '(1 2 3 4 5))
[Enter (rev '(2 3 4 5))
[Enter (rev '(3 4 5))
[Enter (rev '(4 5))
[Enter (rev '(5))
[Enter (rev '())
 Leave rev '()]
 Leave rev '(5)]
 Leave rev '(5 4)]
 Leave rev '(5 4 3)]
 Leave rev '(5 4 3 2)]
 Leave rev '(5 4 3 2 1)]
(5 4 3 2 1)
