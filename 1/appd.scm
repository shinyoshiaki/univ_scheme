(define (appd lst1 lst2)
(if (null? lst1)
    lst2
    (cons (car lst1) (appd (cdr lst1) lst2))))
