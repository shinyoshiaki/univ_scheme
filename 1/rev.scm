(define  (rev  lst)
    (define  (_rev  before  after)
        (if  (null?  before)
             after
             (_rev  (cdr before)  (cons  (car before)  after) ) ) )
    (_rev  lst  '() ) )