(define I2 (lambda (x) x))
(I2 1000)

(define fact
    (lambda (n)
        (if(= n 0) 1 (* n ( - n 1)))))

(fact 10)