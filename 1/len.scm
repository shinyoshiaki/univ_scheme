(define (len lst)
    (if (null? lst)
        0
        (+ 1 (len (cdr lst)))))
