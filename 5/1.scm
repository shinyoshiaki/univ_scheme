(define product
    (lambda (ls)
      (call-with-current-continuation
       (lambda (break)
         (letrec ((f (lambda (ls)
               (cond
                ((null? ls) 1)
                ((= (car ls) 0) (break 0))
                (else (* (car ls) (f (cdr ls))))))))
       (f ls))))))
(product '(1 2 3 4 5))

(define product
    (lambda (ls k)
      (let ((break k))
        (let f ((ls ls) (k k))
        (cond
        ((null? ls) (k 1))
        ((= (car ls) 0) (break 0))
        (else (f (cdr ls) (lambda (x) (k (* (car ls) x))))))))
        ))
(product '(1 2 3 4 5 6) (lambda (x) x))

(define integer-divide
    (lambda (x y success failure)
     (if (= y 0) (failure "divide by zero")
       (let ((q (quotient x y)))
         (success q (- x (* q y)))))))
(integer-divide 10 3 list (lambda (x) x))

(define inf-list
    (lambda (n ls)
      (cons n (delay (inf-list (+ 1 n) ls)))))  
(define lazy-cdr
    (lambda (ls) (force (cdr ls))))
(define il (delay (inf-list 5 '())))
(car (lazy-cdr (lazy-cdr (force il))))
