(define foldr
  (lambda (f e xs)
    (if (null? xs) e
	(f (car xs) (foldr f e (cdr xs))))))

(define foldl
  (lambda (f e xs)
    (if (null? xs) e
	(foldl f (f e (car xs)) (cdr xs)))))


;; 先頭から check したいのだったら foldl のほうがいいじゃん
(define index-5
  (lambda (n xs)
    (cdr
     (foldl (lambda (x y)
	      (if (= (car x) n)
		  (cons (+ 1 (car x)) y)
		  (cons (+ 1 (car x)) (cdr x))))
	    (cons 0 #f)
	    xs))))

;; 見つかったら脱出
(define index-6
  (lambda (n xs)
    (call-with-current-continuation
     (lambda (k)
       (cdr
	(foldl (lambda (x y)
		 (if (= (car x) n)
		     (k y)
		     (cons (+ 1 (car x)) (cdr x))))
	       (cons 0 #f)
	       xs))))))
