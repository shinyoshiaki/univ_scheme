;;; cps 版の fold では func は 継続 も受け取る（3 引数にする）
;;; func は値と継続のペアを返すように変更

(define foldr-cps
  (lambda (func ini lst k)
    (if (null? lst) (k ini)
	(foldr-cps func ini (cdr lst)
		   (lambda (x)
		     (let ((v (func (car lst) x k)))
		       ((cdr v) (car v))))))))

(define index-foldr-cps
  (lambda (n xs k)
    (let ((break k))
      (let ((f (lambda (x y k)
		 (if (= n (car y))
		     (cons x break)	; 継続を break に
		     (cons (cons (- (car y) 1) (cdr y)) k))))) ; そのまま
	(foldr-cps f (cons (- (length xs) 1) #f) xs k)))))


;;; 面倒くさいので fold の定理を使うと
(define foldl-cps
  (lambda (func ini lst k)
    (foldr-cps (lambda (x y k) (func y x k)) ini (reverse lst) k)))

;;; over index の場合に type mismatch にならないように (cons n y) を return
;;; こうしておけば，index-foldl-cps の return 直前に cdr すればよい
;;; おのままなら 継続を cdr にしてコールするとか
(define index-foldl-cps
  (lambda (n xs k)
    (let ((break k))
      (let ((f (lambda (x y k)
		 (if (= n (car x))
		     (cons (cons n y) break) ; 継続を break に
		     (cons (cons (+ 1 (car x)) #f) k))))) ; そのまま
	(foldl-cps f (cons 0 #f) xs k )))))

