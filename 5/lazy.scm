

(define inf-list
  (lambda (n ls)
    (cons n (delay (inf-list (+ 1 n) ls)))))

(define lazy-cdr
  (lambda (ls) (force (cdr ls))))


;;;  こんなかんじ

(define il (delay (inf-list 5 '())))	; 5 から始まる無限リスト
(car (lazy-cdr (lazy-cdr (force il))))	; ふたつ先の要素は?
