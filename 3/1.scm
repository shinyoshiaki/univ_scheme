;foldr
;授業中の定義に従って作成する。
(define foldr
    (lambda (f e xs) 
        (if(equal? xs '())
        e
        (f (car xs) (foldr f e (cdr xs))))))

;foldl
;授業中の定義に従って作成する。
(define foldl
    (lambda (f e xs) 
        (if(equal? xs '())
        e
        (foldl f (f e (car xs)) (cdr xs)))))

;append
;畳み込みを用いて各要素をコンセルにつなげる
(define append_foldr
    (lambda (m n)
    (foldr cons n m)))

(append_foldr '(1 2 3) '(4 5))

;length_foldr
;長さを数え上げる内部関数を定義して畳み込む
(define length_foldr
    (lambda (xs)
    (let ((increment (lambda (x n) (+ 1 n))))
    (foldr increment 0 xs))
    ))
(length_foldr '(1 2 3))

;reverse_foldr
;リストを逆向きに畳み込む
(define reverse_foldr
    (lambda (xs)
    (let ((snoc (lambda (x xs) (append_foldr xs (list x)))))
    (foldr snoc '() xs))))
(reverse_foldr '(1 2 3))

;filter
;フィルター関数fを畳み込みに適合したリストを返す。
(define filter
    (lambda(f xs)
    (foldr (lambda (x y) (if (f x) (cons x y) y)) '() xs)))
(filter even? '(1 2 4 5 32))

;map-local
;リストに関数を畳み込んで適応したリストを返す。
(define map-local
    (lambda (f xs)
        (foldr (lambda (x y) (cons (f x) y)) '() xs)))
(map-local (lambda (x) (+ 1 x)) '(2 5 10))
