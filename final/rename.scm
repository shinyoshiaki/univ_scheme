(define rename
    (lambda (x z lexp)
     (cond
      ((AND (eq? x (car lexp)) (eq? '() (cdr lexp)))
       z)
      ((AND (not (eq? x (car lexp)))
            (not (eq? 'L (car lexp)))
            (not (list? (car lexp))))
       lexp)
      ((AND (eq? 'L (car lexp)) (eq? x (car (cdr lexp))))
       (cons 'L (cons z (rename x z (cdr (cdr lexp))))))
      ((AND (eq? 'L (car lexp))
            (not (eq? x (car (cdr lexp))))
            (cons 'L (cons (car (cdr lexp)) (rename x z (cdr (cdr lexp)))))))
      (else
       (cons (rename x z (car lexp)) (rename x z (cdr lexp)))))))