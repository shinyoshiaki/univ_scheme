(define rename
    (lambda (x z lexp)
        (if (symbol? (car lexp))
            (if (eq? (car lexp) 'L )
                (if(eq? x (car (cdr lexp)))
                    (cons 'L (cons z (rename x z (cdr (cdr lexp)))))
                    (cons 'L (cons (car (cdr lexp)) (rename x z (cdr (cdr lexp))))))
                (if (eq? (car lexp) x)
                    (list z)
                    (list (car lexp))))
            (cons (rename x z (car lexp)) (rename x z (cdr lexp))))))

(rename 'x 'z '(L x L y L z (x) (y) z))