(define rename
    (lambda (target replacement source)
     (if (eq? source target)
      replacement
      (if (null? source)
       '()
       (if (eq? target (car source))
        (cons replacement (rename target replacement (cdr source)))
        (if (list? (car source))
         (cons (rename target replacement (car source))
               (rename target replacement (cdr source)))
         (cons (car source) (rename target replacement (cdr source)))))))))
(rename 'x 'z '(L x L y L z (x) (y) z))
(rename 'x 'z '(((l x l y l z ((x) z) (y) z) l x l y x) l x l y x))