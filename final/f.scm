(define rename
    (lambda (target replacement source)
     (if (eq? source target)
      replacement
      (if (null? source)
       '()
       (if (eq? target (car source))
        (cons replacement (rename target replacement (cdr source)))
        (if (list? (car source))
         (cons (rename target replacement (car source))
               (rename target replacement (cdr source)))
         (cons (car source) (rename target replacement (cdr source)))))))))
(rename 'x 'z '(L x L y L z (x) (y) z))
(rename 'x 'z '(((l x l y l z ((x) z) (y) z) l x l y x) l x l y x))

(define rename
      (lambda (x z lexp)
            (cond
                  ((AND (eq? x (car lexp)) 
                        (eq? '() (cdr lexp)))
                  z)
            
                  ((AND (not (eq? x (car lexp)))
                        (not (eq? 'L (car lexp)))
                        (not (list? (car lexp))))
                  lexp)
            
                  ((AND (eq? 'L (car lexp)) 
                        (eq? x (car (cdr lexp))))
                  (cons 'L (cons z (rename x z (cdr (cdr lexp))))))
            
                  ((AND (eq? 'L (car lexp))
                        (not (eq? x (car (cdr lexp))))
                  (cons 'L (cons (car (cdr lexp)) (rename x z (cdr (cdr lexp)))))))
                  
                  (else
                  (cons (rename x z (car lexp)) (rename x z (cdr lexp))))
            )
      )
)
(define substitution
      (lambda (x q lexp)
            (cond
                  ((AND (eq? x (car lexp)) 
                        (eq? '() (cdr lexp)))
                  q)
                  ((AND (not (eq? x (car lexp)))
                        (not (eq? 'L (car lexp)))
                        (not (list? (car lexp))))
                  lexp)
                  (else
                  (cons (rename x z (car lexp)) (rename x z (cdr lexp))))
            )
      )
)
; (rename 'x 'z '(L x L y L z (x) (y) z))

; (define substitution 
;     (let ((symbols list))
;     (lambda (target replacement source) 
;         (if (eqv? source target)
;             replacement
;             (if (null? source)
;                 '()
;                 (if (equal? target (car source))
;                     (cons replacement (substitution  target replacement (cdr source)))
;                     (if (list? (car source))
;                         (cons (substitution target replacement (car source)) (substitution target replacement (cdr source)))
;                         (cons (car source) (substitution target replacement (cdr source)))                        
;                         ))))
;                         ))
;     )

; (define substitution
;     (let ((symbols list))
;     (lambda (target replacement source)
;      (if (eq? source target)
;       replacement
;       (if (null? source)
;        '()
;        (if (eq? target (car source))
;         (cons replacement (substitution target replacement (cdr source)))
;         (if (list? (car source))
;          (cons (substitution target replacement (car source))
;                (substitution target replacement (cdr source)))
;          (cons (car source) (substitution target replacement (cdr source)))))))))
; )
; (substitution 'x 'z '(L x L y (y) x))