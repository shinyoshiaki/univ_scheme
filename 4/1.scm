; 通常の再帰による反転処理関数
(define rev
    (lambda (l) 
        (if (null? l) 
            '()
            (append (rev (cdr l)) (list (car l))))))

; (rev '(1 3 4 5 6))

; 末尾再帰による反転処理関数
; 定義に従って実装した
(define rev_tail
    (lambda(l)
    (letrec ((rev_r (lambda (xs ys)
                    (if (null? xs) ys
                        (rev_r (cdr xs) (cons (car xs) ys))))))
            (rev_r l '()))))

; (rev_tail '(1 3 4 5 6))